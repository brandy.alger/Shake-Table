
// #define is used to set constants that can be used globally (anywhere in the code).
// A #define should be formatted as shown "#define [constant name] [value]". You can see some examples below.
// #define works by each instance of the constant name is replaced by the value. So the first #define line would replace each occurence of "MIN" with "20"

#define MIN 30
#define MAX 200
#define MAX_STEP 5
#define MOTOR_PIN 3
#define POT_PIN A0
#define BUTTON_1 10
#define BUTTON_2 11
#define BUTTON_3 12

void setup() {
    pinMode(MOTOR_PIN, OUTPUT);   // Setting the motor to an output.
    digitalWrite(MOTOR_PIN, LOW); // Make sure that the motor is not on at the start.
    pinMode(POT_PIN, INPUT);      // Setting the potentiometer to an input.
    Serial.begin(9600);
    Serial.println("Setup finished");
}

long in = 0;
int out = 0;

void loop() {

  in = map(analogRead(POT_PIN), 0, 1023, 0, MAX); // Reads the analog value and scales it to fit in the range (0, MAX)

  int diff = in-out; // Finds the difference between the input and current output

  diff = constrain(diff, -MAX_STEP, MAX_STEP); // Limits the difference to be between -MAX_STEP and MAX_STEP. This is used to limit how fast the motor can turn power up or down.

  out = out + diff;

  if (out >= MIN) {
    // Setting power to motor if it is above the minimum value.
    analogWrite(MOTOR_PIN, out);
  }
  else {
    // Turn off motor if power is too low.
    analogWrite(MOTOR_PIN, 0);
    out = MIN - 1;
  }

  if (digitalRead(BUTTON_1)) {
    quake_1();
  }
  if (digitalRead(BUTTON_2)) {
    quake_2();
  }
  if (digitalRead(BUTTON_3)) {
    quake_3();
  }
  
  delay(50); // Wait for 0.05 seconds (50ms) This is used so it will only loop 20 times a second.
}


// You can program up to three of your own earthquakes in the functions quake_1(), quake_2() and quake_3().
// Look at quake_3() for an example on how to program a earthquake.
void quake_1() {
  
}


void quake_2() {
}


void quake_3() {

  // These two lines will set the output variable to 0 then write that to the motor.
  out = 0;                      
  analogWrite(MOTOR_PIN, out);

  // This will do nothing for 2000ms, 2 Seconds.
  delay(2000);

  // The change_power function can be used to change the power of the motor over a set amount of time. 
  // The first parameter is what the power should be when finished and the second parameter is how long it should take to get to that value in ms.

  
  // Because the motor power was set as 0 previously, this will rise the motor power to 100 over 2000ms
  change_power(100, 2000);

  // This will reduce the power to 50 over 2000ms
  change_power(50, 2000);

  // This will increase the power to 250 over 3000ms
  change_power(250, 3000);

  // This will just wait for 2000ms while the motor is at power 250
  delay(2000);

  // This will reduce the power to 0 over 5000ms.
  change_power(0, 5000);

  // When the earthquake has finished it will go back to the main loop.
}

void change_power(int power, long time) {
  int startPower = out;
  int powerDiff = power - startPower;
  for (int i = 0; i <= 20; i++) {
    out = startPower + i*powerDiff/20;
    analogWrite(MOTOR_PIN, out);
    delay(time/20);
  }
}
