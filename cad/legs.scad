use <threads.scad>
use <logo.scad>

d = 20;
$fs = 0.5;
module leg() {
    h = 90;
    union() {
        translate([0, 0, 0]) metric_thread(d, 2, 4);
        
        translate([0, 0, 4]) cylinder(d = d, h = h-10);
        
        translate([0, 0, h-13]) cylinder(d1=d, d2=d+4, h=2);
        translate([0, 0, h-11]) cylinder(d=d+4, h=2);
        translate([0, 0, h-9]) cylinder(d2=d, d1=d+4, h=2);
        
        translate([0, 0, h-6]) metric_thread(d, 2, 7.5);
    }
}    

//!leg();

module top_cap() { 
    difference() {
        cylinder(h=10, d=1.5*d, $fa=60);
        translate([0, 0, 8]) scale([3, 3, 2]) logo(5);
        translate([0, 0, -1]) metric_thread(d+1, 2, 7);
    }
}
//!top_cap();
  
module botton_cap() {
    difference() {
        cylinder(h=10, d=1.5*d, $fa=60);
        translate([0, 0, 6]) metric_thread(d+1.2, 2, 6);
        
        translate([0, 0, -1]) cylinder(d=8, h=8);
        translate([0, 0, -0.01]) cylinder(d1=12, d2=8, h=2);
        
        translate([0, 0, 3]) rotate([90, 0, 0]) cylinder(d=1.8, h=50, center=true);
    }
}

//!botton_cap();
leg();