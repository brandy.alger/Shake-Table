// Front plate
use <logo.scad>

wood_gap = 5.5;

$fs = 0.8;

bt_led_h = 5.6;

bt_d = 4.7;
bt1 = 11.5;
bt2 = 48.8;
bt3 = 58.6;
bt4 = 68.8;

led_d = 5.6;
led1 = 20.2;

hole1 = 3.75;
hole2 = 77.1;

pot_d = 8;
pot_x = 32.9;
pot_y = 14.4;

switch_d = 15;
switch_x = -2;
switch_y = 16; //pow_y+12;

pow_d = 8.5;
pow_x = -10;
pow_y = switch_y-12;

x = 6+pow_d/2-pow_x+hole2+4;

union() {
    // Front panel
    difference() {
        translate([pow_x-pow_d/2-6, -2, 0]) cube([x, 35, 2]);

        // Buttons
        translate([bt1, bt_led_h, -1]) cylinder(d = bt_d, h = 5);
        translate([bt2, bt_led_h, -1]) cylinder(d = bt_d, h = 5);
        translate([bt3, bt_led_h, -1]) cylinder(d = bt_d, h = 5);
        translate([bt4, bt_led_h, -1]) cylinder(d = bt_d, h = 5);
        
        // LED
        translate([led1, bt_led_h, -1]) cylinder(d = led_d, h = 5);
        
        // POT
        translate([pot_x, pot_y, -1]) cylinder(d = pot_d, h = 5);
        
        // Power
        translate([pow_x, pow_y, -1]) cylinder(d = pow_d, h = 5);
        
        // Switch
        translate([switch_x, switch_y, -1]) cylinder(d = switch_d, h = 5);
        translate([switch_x-switch_d/2, switch_y, -1]) cube([2, 1.5, 10], center=true);
        
        
        logo1();
    }
   
    // Screw_tabs.
    union() {
        y1 = 0;
        y2 = 1.7+2;
        x1 = hole1;
        x2 = hole2;
        
        translate([x1, y1, 0]) screw_tabs();
        translate([x1, y2, 0]) screw_tabs();
        translate([x2, y1, 0]) screw_tabs();
        translate([x2, y2, 0]) screw_tabs();
    }    
    
    // Connector
    difference() {
        hull() {
            translate([pow_x-pow_d/2-6, 24, -18]) cube([x, 2+wood_gap+2, 20]);
            translate([pow_x-pow_d/2-6, -2, 0]) cube([x, 2+wood_gap+2, 2]);
        }
        translate([pow_x-pow_d/2-6, 24+2, -18-2]) cube([x, wood_gap, 20]);  //
        translate([pow_x-pow_d/2-6+2, -2, -18]) cube([x-2-2, 26, 21]);
        
        translate([0, 0, -10]) cube([100, 1.7+2, 10]);
        
        logo1();
    }
}

 // screw tabs
module screw_tabs() {
    difference() {
        hull() {
            translate([0, 0, -3.6])
            rotate([90, 0, 0])
            cylinder(d = 8, h = 2);
            translate([0-4, -2, 0]) cube([8, 2, 2]);
        }
        translate([0, 0, -3.6])
        rotate([90, 0, 0])
        cylinder(d = 3.5, h = 2);
    }
}

module logo1() {
    translate([58, 21, 1]) scale(3) logo();
}

