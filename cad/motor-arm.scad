$fs = 0.6;
$fa = 1;

r1 = 5;
r2 = 1.3;
l1 = 40;
h1 = 10;
t1 = 2;

difference() {
    hull() {
        cylinder(r = r1, h = h1);
        translate([l1, 0, 0])
        cylinder(r = r1, h = h1);
    }
    translate([l1, 0, t1])
    cylinder(r=l1-5, h = h1-2*t1);
    
    translate([l1-4, 0, h1-t1])
    cylinder(r = 3, h = 10);
    
    cylinder(r = r2, h = h1);
}    

translate([l1, 0, 0])
hull() {
    cylinder(r=r1-2, h = t1);
    
    translate([-4, 0, h1-t1])
    cylinder(r=r1-3, h = t1);
}

